import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;

public class CalendarViewer 
{
	private GregorianCalendar gc = new GregorianCalendar();
	private ArrayList<JButton> dayButtons = new ArrayList<JButton>();
	DefaultTableModel table;
	private JLabel label;
	private JLabel label2;
	private HashMap<String, Day> map = new HashMap<String, Day>();
	
	private String month = gc.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
	private int year = gc.getInstance().get(Calendar.YEAR);
	private int day = gc.getInstance().get(Calendar.DAY_OF_MONTH);
	private Day selected;

	
	public CalendarViewer()
	{
		JFrame frame = new JFrame("Calendar");
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setSize(400, 450);
		frame.setVisible(true);
		
		JLabel currentDate = new JLabel("Today: " + month + " " + day + ", " + year);
		JButton monthDown = new JButton("<");
		JButton monthUp = new JButton(">");
		JButton yearDown = new JButton("v");
		JButton yearUp = new JButton("^");
		
		
		ActionListener listener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(e.getSource() == monthDown)
				{
					gc.add(Calendar.MONTH, -1);
					updateCalendar();
					
				}
				if(e.getSource() == monthUp)
				{
					gc.add(Calendar.MONTH, +1);
					updateCalendar();
					
				}
				if(e.getSource() == yearDown)
				{
					gc.add(Calendar.YEAR, -1);
					updateCalendar();
				}
				if(e.getSource() == yearUp)
				{
					gc.add(Calendar.YEAR, +1);
					updateCalendar();
				}

			}
		};
		monthDown.addActionListener(listener);
		monthUp.addActionListener(listener);
		yearDown.addActionListener(listener);
		yearUp.addActionListener(listener);
		
		label = new JLabel();
		label2 = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setHorizontalAlignment(SwingConstants.CENTER);

		label.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				DefaultListModel<String> monthTask = new DefaultListModel<>();
				Set<String> keys = map.keySet();
				for(String key : keys)
				{
					if(key.contains(month))
					{
						Day single = map.get(key);
						if(single.getYear() == year)
						{
							DefaultListModel<String> dayTasks = single.getTasks();
							for(int i = 0; i < dayTasks.size(); i++)
							{
								monthTask.addElement(dayTasks.get(i) + " " + key);
							}
						}
					}
				}
				JFrame monthFrame = new JFrame("Tasks for " + month);
				JList<String> monthList = new JList<>(monthTask);
				JScrollPane taskPane = new JScrollPane(monthList);
				taskPane.setPreferredSize(new Dimension(200,200));
				monthFrame.add(taskPane);
				monthFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				monthFrame.pack();
				monthFrame.setVisible(true);
			}
		});
		
		
		String[] days = {"Sun" , "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
		table = new DefaultTableModel(null, days)
		{
			@Override
			public boolean isCellEditable(int row, int column) {
				 //all cells false
				 return false;
			}
		};
		JTable calendar = new JTable(table);

		
		selected = new Day(day, month, year);
		selected.createFrame();
		map.put(gc.getInstance().get(Calendar.DAY_OF_MONTH) + month + Integer.toString(year), selected);
		selected.getFrame().openFrame();

		calendar.setRowSelectionAllowed(false);
		calendar.setRowHeight(57);
		calendar.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				selected.getFrame().closeFrame();
				int row = calendar.rowAtPoint(e.getPoint());
				int col = calendar.columnAtPoint(e.getPoint());
				String date = calendar.getValueAt(row, col) + month + Integer.toString(year);
				if(!map.containsKey(date))
				{
					map.put(date, new Day((int)calendar.getValueAt(row, col), month, year));
					map.get(date).createFrame();
				}
				selected = map.get(date);
				selected.getFrame().openFrame();
			}
		});

		JPanel searchPanel = new JPanel();
		String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		JComboBox<String> monthField = new JComboBox<>(months);
		JTextField dayField = new JTextField("DD", 4);
		JTextField yearField = new JTextField("YYYY", 6);
		JButton searchButton = new JButton("Go To Date");

		searchButton.addActionListener(event -> pressSearch((String)monthField.getSelectedItem(), dayField.getText(), yearField.getText()));

		searchPanel.add(monthField);
		searchPanel.add(dayField);
		searchPanel.add(yearField);
		searchPanel.add(searchButton);			
		
		JPanel datePanel = new JPanel();
		datePanel.setLayout(new BorderLayout());
		datePanel.add(searchPanel, BorderLayout.CENTER);
		datePanel.add(currentDate, BorderLayout.SOUTH);
		
		
		JPanel panel = new JPanel();
		JPanel panel2 = new JPanel();
		panel2.add(label2, BorderLayout.CENTER);
		panel2.add(yearDown, BorderLayout.WEST);
		panel2.add(yearUp, BorderLayout.EAST);
		panel.setLayout(new BorderLayout());
		panel.add(monthDown, BorderLayout.WEST);
		panel.add(monthUp, BorderLayout.EAST);
		panel.add(label, BorderLayout.CENTER);
		panel.add(panel2, BorderLayout.NORTH);
		
		
		
		JScrollPane scrollPane = new JScrollPane(calendar);
		frame.add(panel, BorderLayout.NORTH);
		frame.add(scrollPane, BorderLayout.CENTER);
		frame.add(datePanel, BorderLayout.SOUTH);
		updateCalendar();
		
		
	}
	
	public void updateCalendar()
	{
		int totalDays = gc.getActualMaximum(Calendar.DAY_OF_MONTH);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		month = gc.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
		label.setText(month);
		int startDay = gc.get(Calendar.DAY_OF_WEEK);
		int weeks = gc.getActualMaximum(Calendar.WEEK_OF_MONTH);
		
		year = gc.get(Calendar.YEAR);
		label2.setText("" + year);
	
		table.setRowCount(0);
		table.setRowCount(weeks);
	
		int i = startDay-1;
		for(int n = 1; n <= totalDays; n++)
		{
			table.setValueAt(n, i / 7 , i % 7 );  
			
			i = i + 1;
		}

	}		

	public void pressSearch(String selMonth, String selDate, String selYear)
	{
		String date = selDate + selMonth + selYear;
		selected.getFrame().closeFrame();
		if(!map.containsKey(date))
			{
				map.put(date, new Day(Integer.parseInt(selDate), selMonth, Integer.parseInt(selYear)));
				map.get(date).createFrame();
			}
		selected = map.get(date);
		selected.getFrame().openFrame();
		System.out.println(date);
	}
	
	public static void main(String[] args)
	{
		CalendarViewer cv = new CalendarViewer();
	}
	
}
