import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.ArrayList;

public class Day
{
    private int day;
    private String month;
    private int year;
    private dayFrame taskFrame;

    public Day(int day, String month, int year)
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void createFrame()
    {
        taskFrame = new dayFrame(month + " " + Integer.toString(day) + ", " + Integer.toString(year));
    }

    public dayFrame getFrame()
    {
        return taskFrame;
    }

    public int getDate()
    {
        return day;
    }

    public int getYear()
    {
        return year;
    }

    public DefaultListModel<String> getTasks()
    {
        DefaultListModel<String> taskList = taskFrame.getList();
        return taskList;
    }
}