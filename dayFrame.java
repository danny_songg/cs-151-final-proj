import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.swing.*;
import java.util.ArrayList;
import java.util.stream.Stream;

public class dayFrame
{

    //private String date;
    private DefaultListModel<String> dayTasks = new DefaultListModel<>();
    private JFrame frame;

    public dayFrame(String date)
    {
        frame = new JFrame();
        JList<String>  taskList = new JList<>(dayTasks);
       
        
        JLabel dayText = new JLabel("Day: " + date);
        JTextField textField = new JTextField();

        JScrollPane listPanel = new JScrollPane(taskList);;
        listPanel.setPreferredSize(new Dimension(450, 225));

        JButton deleteButton = new JButton("Delete Task");
        JButton editButton = new JButton("Edit Task");
        JButton addButton = new JButton("Add Task");
        JButton exportButton = new JButton("Export");

        addButton.addActionListener(event -> pressAdd(textField.getText()));
        deleteButton.addActionListener(event -> pressDelete(taskList.getSelectedIndex()));
        editButton.addActionListener(event -> pressEdit(taskList.getSelectedIndex(), textField.getText()));
        exportButton.addActionListener(event -> pressExport(date, taskList));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(375, 40));

        buttonPanel.add(deleteButton);
        buttonPanel.add(editButton);
        buttonPanel.add(addButton);
        buttonPanel.add(exportButton);

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.add(dayText);
        frame.add(listPanel);
        frame.add(textField);
        frame.add(buttonPanel);
        frame.setLocation(400,0);
    
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
    }

    public DefaultListModel<String> getList()
    {
        return dayTasks;
    }
    
    public void pressAdd(String task)
    {
        System.out.println("Adding " + task);
        dayTasks.addElement(task);
    }

    public void pressDelete(int index)
    {
        System.out.println("Deleting index " + index);
        dayTasks.remove(index);
    }

    public void pressEdit(int index, String task)
    {
        System.out.println("Changing " + index +" to " + task);
        dayTasks.set(index, task);
    }

    public void pressExport(String date, JList<String> taskList)
    {
        String month = date.split(" ")[0];
        String day = date.split(" ")[1];
        day = day.substring(0, day.length()-1);
        String year = date.split(" ")[2];
        File newTextFile = new File(month + "_" + day + "_" + year + ".txt");  
        try 
        {
            FileWriter fw = new FileWriter(newTextFile);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("Tasks for " + date);
            bw.newLine();
            for(int i = 0; i < taskList.getModel().getSize(); i++)
            {
                String text = taskList.getModel().getElementAt(i);
            //PrintWriter out = new PrintWriter(month + "_" + day + "_" + year + ".txt");
            
                bw.write(text);
                bw.newLine();
                    
            }
            bw.close();
            fw.close();
        }
        catch (IOException e1) 
        {
            e1.printStackTrace();
        }
        System.out.println("Exporting Tasks");
    }

    public void closeFrame()
    {
        frame.setVisible(false);
        frame.dispose();
    }

    public void openFrame()
    {
        frame.setVisible(true);
    }
}